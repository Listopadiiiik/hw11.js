
    let eye = document.querySelectorAll('.fa-eye-slash');
    let pass1 = document.querySelector('.pass1');
    let pass2 = document.querySelector('.pass2');
    let index = false;

function open(event){
    document.querySelectorAll('.fa-eye-slash').forEach(item => item.removeEventListener('click', close));
    let target = event.target;
    target.classList.remove('fa-eye-slash');
    target.previousElementSibling.removeAttribute('type');
    target.classList.add('fa-eye');
    target.previousElementSibling.setAttribute('type', 'text');
    document.querySelectorAll('.fa-eye').forEach(item => item.addEventListener('click', close));
}

function close(event){
    let target = event.target;
    target.classList.remove('fa-eye');
    target.previousElementSibling.removeAttribute('type');
    target.classList.add('fa-eye-slash');
    target.previousElementSibling.setAttribute('type', 'password');
}

eye.forEach(item => item.addEventListener('click', open));

let button = document.querySelector('.btn');

function insert(){
    pass2.insertAdjacentHTML('afterend', `<span>Потрібно ввести однакові значення</span>`);
    document.querySelector('span').style.color = 'red';
}

function comparePass(event){
    if (pass1.value === pass2.value){
        alert("You are welcome");
    } 
    if (pass1.value !== pass2.value) {  
        if(!index){
            insert();
            index = true;
        }
} 
}

button.addEventListener('click', comparePass);
